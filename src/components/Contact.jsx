import React from 'react'
import {
  FaFacebookSquare,
  FaInstagramSquare,
  FaLinkedin,
} from 'react-icons/fa';

function Contact() {
  return (
    <div className='w-full py-[5rem] px-4 flex flex-col justify-center min-h-screen overflow-hidden'>
      <div className='max-w-[1240px] px-10 py-40 m-auto bg-[rgba(255, 255, 255, 0.25)] grid md:grid-rows-3 gap-8 justify-center rounded-md shadow-xl shadow-rose-600/40 ring-2 ring-indigo-600'>
        <h1 className=" text-3xl font-semibold text-center text-[#CB4335] uppercase">
          Contact Me
        </h1>
        <div className="mbsc-col-12 mbsc-col-md-6 mbsc-col-lg-3">
            <label
              for="email"
              className="block text-sm font-semibold text-gray-800">
                Full Name
            </label>
            <input className='block w-full px-4 py-2 mt-2 text-indigo-700 bg-white border rounded-md focus:border-indigo-400 focus:ring-indigo-300 focus:outline-none focus:ring focus:ring-opacity-40' label="Full Name" inputStyle="box" labelStyle="floating" placeholder="Enter Full Name" />
        </div>
        <div className="mbsc-col-12 mbsc-col-md-6 mbsc-col-lg-3">
            <label
              for="email"
              className="block text-sm font-semibold text-gray-800">
                Email Address
            </label>
            <input className='block w-full px-4 py-2 mt-2 text-indigo-700 bg-white border rounded-md focus:border-indigo-400 focus:ring-indigo-300 focus:outline-none focus:ring focus:ring-opacity-40' label="Password" inputStyle="box" labelStyle="floating" placeholder="Enter Email Address" passwordToggle="true" />
        </div>
        <div className="mbsc-col-12 mbsc-col-lg-6">
            <label
              for="email"
              className="block text-sm font-semibold text-gray-800">
                Suject
            </label>
            <input className='block w-full px-4 py-2 mt-2 text-indigo-700 bg-white border rounded-md focus:border-indigo-400 focus:ring-indigo-300 focus:outline-none focus:ring focus:ring-opacity-40' label="Address" inputStyle="box" labelStyle="floating" placeholder="Enter Subject" />
        </div>
        <div className="mbsc-col-12 mbsc-col-lg-6">
            <label
              for="email"
              className="block text-sm font-semibold text-gray-800">
                Message
            </label>
            <input className='block w-full px-4 py-2 mt-2 text-indigo-700 bg-white border rounded-md focus:border-indigo-400 focus:ring-indigo-300 focus:outline-none focus:ring focus:ring-opacity-40' label="Address" inputStyle="box" labelStyle="floating" placeholder="Enter Message" />
        </div>
        <div className="mt-6">
            <button className="w-full px-4 py-2 tracking-wide text-white transition-colors duration-200 transform bg-[#CB4335] rounded-md hover:bg-indigo-600 focus:outline-none focus:bg-indigo-600">
                Send Message
            </button>
        </div>
        <div className='flex justify-center md-[70%] py-6 text-[#CB4335]'>
            <a className='transition-colors duration-200 transform rounded-md hover:bg-indigo-600 focus:outline-none focus:bg-indigo-600' href="https://www.facebook.com/analy.gamo"><FaFacebookSquare size={30} /></a>
            <a className='transition-colors duration-200 transform rounded-md hover:bg-indigo-600 focus:outline-none focus:bg-indigo-600' href="https://www.instagram.com/analyomania/"><FaInstagramSquare size={30} /></a>
            <a className='transition-colors duration-200 transform rounded-md hover:bg-indigo-600 focus:outline-none focus:bg-indigo-600' href="https://www.linkedin.com/in/analy-gamo-52942b1a8/"><FaLinkedin size={30} /></a>
        </div>
      </div>
    </div>
    
    
  )
}

export default Contact