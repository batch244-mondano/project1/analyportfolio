import React from 'react';
import analy from '../photos/ylana.gif';

const About = () => {
  return (
    <div className='w-full py-16'>
        <div className='max-w-[1240px] mx-auto grid lg:grid-cols-2'>
            <img src={analy} className='md:col-span-1' alt="analy" />
            <div>
                <h1 className='md:mt-[15px] font-bold md:text-4xl sm:text-2xl text-xl md:py-6 sm:py-14 text-center'>About Me</h1>
                <p className='py-4 sm:py-6'>
                As a virtual assistant with 3 years of experience, I have gained exceptional skills in managing various tasks, ranging from administrative tasks to using Excel for data entry, data analysis, and creating spreadsheets for business operations, email management, and customer support. I am a self-starter, detail-oriented, and I work with precision to deliver quality work within deadlines. My ability to prioritize tasks and communicate effectively with clients has contributed to my success in providing exceptional virtual assistance services. I am passionate about helping businesses and individuals achieve their goals, and I am committed to providing efficient and effective virtual assistance services to meet their needs.
                </p>
                <h3 className='mt-[15px] font-bold text-xl'>Skills</h3>
                <div className='text-center py-4 '>
                    <div className='flex'>MS Excel</div>
                    <div className='flex'>Customer Service</div>
                    <div className='flex'>Technical Support</div>
                    <div className='flex'>Data Entry</div>
                </div>
                <h3 className='mt-[15px] font-bold text-xl'>Education</h3>
                <h4 className='mt-[15px] text-lg'>Bachelor of Elementary Education - <span className='font-bold text-lg text-[#CB4335]'>Gingoog Christian College</span></h4>
            </div>

        </div>
    </div>
  )
}

export default About;