import React, {useState} from 'react';
import {AiOutlineClose, AiOutlineMenu} from 'react-icons/ai';

const Navbar = () => {
	const [nav, setNav] = useState(false);

	const handleNav = () => {
		setNav(!nav)
	}

	return (
		<div className='flex text-dark justify-between items-center h-24 max-w-[1240px] mx-auto px-4'>
			<h1 className='w-full text-3xl font-bold text-[#CB4335]'>ANALY.</h1>
			<ul className='hidden md:flex'>
				<li className='p-4 font-bold'>
					<a class=" hover:text-[#CB4335]" href="/">Home</a>
				</li>
				<li className='p-4 font-bold'>
					<a class=" hover:text-[#CB4335]" href="/about">About</a>
				</li>
				<li className='p-4 font-bold'>
					<a class=" hover:text-[#CB4335]" href="/portfolio">Portfolio</a>
				</li>
				<li className='p-4 font-bold'>
					<a class=" hover:text-[#CB4335]" href="/contact">Hire!</a>
				</li>
			</ul>
			<div onClick={handleNav} className='block md:hidden'>
				{!nav 
				? 
					<AiOutlineClose size={20}/> 
				: 
				<AiOutlineMenu size={20}/>
				}
				<div className={!nav 
				?
					'fixed rounded-xl left-0 top-0 w-[70%] h-full border-r-gray-900 bg-[#FADBD8] ease-in-out duration-500'
				:
					'fixed left-[-100%]'
			}>
				<h1 className='w-full p-4 text-3xl font-bold text-[#CB4335]'>ANALY.</h1>
				<ul className='pt-24 uppercase text-center font-bold'>
					<li className='p-4 text-dark border-b border-gray-600'>
						<a class=" hover:text-[#CB4335]" href="/">Home</a>
					</li>
					<li className='p-4 text-dark border-b border-gray-600'>
						<a class=" hover:text-[#CB4335]" href="/about">About</a>
					</li>
					<li className='p-4 text-dark border-b border-gray-600'>
						<a class=" hover:text-[#CB4335]" href="/portfolio">Portfolio</a>
					</li>
					<li className='p-4 text-dark'>
						<a class=" hover:text-[#CB4335]" href="/contact">Hire!</a>
					</li>
				</ul>
			</div>
			</div>
			
		</div>
		
	)
}

export default Navbar;