import React from 'react'
import Typed from 'react-typed';
import analy from '../photos/ylana.gif';
import { useNavigate } from 'react-router-dom';
import {
        FaFacebookSquare,
        FaInstagramSquare,
        FaLinkedin,
      } from 'react-icons/fa';
import resume from '../resume.pdf';

const Home = () => {
  let nav = useNavigate();
  return (
    <div className='w-full py-16'>
        <div className='max-w-[1240px] mx-auto grid md:grid-cols-2'>
            <div className="mx-auto flex flex-col justify-center order-last md:order-first text-center">
                <h3 className='font-bold text-xl'>Hi, my name is</h3>
                <h1 className='font-bold md:text-7xl sm:text-6xl text-4xl md:py-6'>Analy Gamo</h1>
                <Typed className='text-[#CB4335] md:text-4xl sm:text-3xl text-xl font-bold py-4'
                        strings={[' Virtual Assistant.']} 
                        typeSpeed={120}
                        backSpeed={100}
                        loop
                />
                <p className='mt-[15px] md:text-2xl sm:text-xl text-xl font-bold'>Reliable, Responsive, and Ready to Help. </p>
                <div>
                <button className='bg-[#CB4335] mx-[5px] md:mx-[-10px] w-[200px] rounded-xl font-medium my-3 py-3'>
                  <a class="hover:text-[#d2ed66]" href={resume} download="Analy Resume">Download CV</a>
                  </button>
                <button className='bg-[#CB4335] hover:text-[#d2ed66] w-[200px] rounded-xl font-medium my-3 py-3 mx-5' onClick={() => {nav("/about")}}>About</button>
                </div>
                <div className='flex justify-center md-[70%] py-6 text-[#CB4335]'>
                    <a href="https://www.facebook.com/analy.gamo"><FaFacebookSquare size={30} /></a>
                    <a href="https://www.instagram.com/analyomania/"><FaInstagramSquare size={30} /></a>
                    <a href="https://www.linkedin.com/in/analy-gamo-52942b1a8/"><FaLinkedin size={30} /></a>
                </div>
                
            </div>
            <img src={analy} className='w-[500px] mx-auto my-4 order-first md:order-last' alt="analy" />
        </div>
    </div>
  )
}

export default Home;
