import React from 'react'
import prosper from '../photos/prosper.jpg';
import techm from '../photos/techm.jpg';
import tronix from '../photos/tronix.jpg';

function Portfolio() {
  return (
    <div className='w-full py-[5rem] px-4'>
        <h1 className='md:mt-[2px] font-bold md:text-4xl sm:text-2xl text-xl md:py-6 sm:py-14 text-center'>Work Experience</h1>
        <div className='max-w-[1240px] mx-auto bg-[rgba(255, 255, 255, 0.25)] grid md:grid-cols-3 gap-8'>
          <div className='w-full shadow-xl flex flex-col p-4 my-4 rounded-lg hover:scale-105 duration-300'>
              <img src={prosper} className='w-80 mx-auto mt-[3rem] bg-white rounded-lg' alt="/" />
              <h2 className='md:text-2xl sm:text-xl font-bold text-center py-8'>Prosper Virtual Assistants</h2>
              <p className='md:text-xl sm:text-lg font-bold text-center'>Position: Assistant Property Manager</p>
              <p className='font-medium text-center'>October 2019 - Present</p>
          </div>
          <div className='shadow-xl flex flex-col p-4 my-4 rounded-lg hover:scale-105 duration-300'>
              <img src={techm} className='w-80 mx-auto mt-[3rem] bg-white rounded-lg' alt="/" />
              <h2 className='md:text-2xl sm:text-xl font-bold text-center py-8'>TechMahindra LTD</h2>
              <p className='md:text-xl sm:text-lg font-bold text-center'>Customer Service Associate</p>
              <p className='font-medium text-center'>April 2017 - May 2019</p>
          </div>
          <div className='shadow-xl flex flex-col p-4 my-4 rounded-lg hover:scale-105 duration-300'>
              <img src={tronix} className='w-80 mx-auto mt-[3rem] bg-white rounded-lg' alt="/" />
              <h2 className='md:text-2xl sm:text-xl font-bold text-center py-8'>Innovatronix</h2>
              <p className='md:text-xl sm:text-lg font-bold text-center'>Store Clerk</p>
              <p className='font-medium text-center'>August 2016 - February 2017</p>
          </div>
          
        </div>
    </div>
  )
}

export default Portfolio